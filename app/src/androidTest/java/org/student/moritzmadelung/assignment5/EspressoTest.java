package org.student.moritzmadelung.assignment5;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;


import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EspressoTest {

    private final String name = "Moritz";
    private final String pw = "pw";

    private final String n1 = "10";
    private final String n2 = "20";
    private final String n3 = "2";

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void elementsExist() throws Exception {
        onView(withId(R.id.btn)).check(matches(isDisplayed()));
    }

    @Test
    public void divideWorks() throws Exception {
        onView(withId(R.id.name)).perform(typeText(name));
        onView(withId(R.id.pw)).perform(typeText(pw));

        onView(withId(R.id.number1)).perform(typeText(n1));
        onView(withId(R.id.number2)).perform(typeText(n2), closeSoftKeyboard());

        onView(withId(R.id.btn)).perform(click());

        onView(withId(R.id.number3)).check(matches(withText(n3)));
    }
}